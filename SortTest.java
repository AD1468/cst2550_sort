
import org.junit.Test;
import static org.junit.Assert.*;

public class SortTest
{

    /**
     * Test of mergeSort method, of class Sort.
     */
    @Test
    public void testMergeSort()
    {
        System.out.println("mergeSort");
        int[] list = {4,7,2,9,0,8,3,6,5,1};
        int[] expected = {0,1,2,3,4,5,6,7,8,9};
        Sort.mergeSort(list);
        assertArrayEquals(expected, list);
    }

    /**
     * Test of merge method, of class Sort.
     */
    @Test
    public void testMergeAlternate()
    {
        System.out.println("merge alternate from list 1 and list 2");
        int[] list1 = {1,3,5,7,9};
        int[] list2 = {0,2,4,6,8};
        int[] destination = new int[10];
        int[] expected = {0,1,2,3,4,5,6,7,8,9};
        Sort.merge(list1, list2, destination);
        assertArrayEquals(expected, destination);
    }
    
    /**
     * Test of merge method, of class Sort.
     */
    @Test
    public void testMergeAll1Then2()
    {
        System.out.println("merge all list 1 then all list 2");
        int[] list1 = {0,1,2,3,4};
        int[] list2 = {5,6,7,8,9};
        int[] destination = new int[10];
        int[] expected = {0,1,2,3,4,5,6,7,8,9};
        Sort.merge(list1, list2, destination);
        assertArrayEquals(expected, destination);
    }
    
    /**
     * Test of merge method, of class Sort.
     */
    @Test
    public void testMergeList1itemList2Empty()
    {
        System.out.println("merge list 1 has 1 item, list 2 is empty");
        int[] list1 = {1};
        int[] list2 = {};
        int[] destination = new int[1];
        int[] expected = {1};
        Sort.merge(list1, list2, destination);
        assertArrayEquals(expected, destination);
    }  

    /**
     * Test of merge method, of class Sort.
     */
    @Test
    public void testMerge()
    {
        System.out.println("merge");
        int[] list1 = {1,8};
        int[] list2 = {2,6};
        int[] destination = new int[4];
        int[] expected = {1,2,6,8};
        Sort.merge(list1, list2, destination);
        assertArrayEquals(expected, destination);
    }    
}
